import flask
from fugue_icons.blueprint import fugue_icons


app = flask.Flask(__name__)
app.register_blueprint(fugue_icons, url_prefix='/fugue-icons')


@app.route("/")
def hello():
    return flask.render_template('index.html')


if __name__ == "__main__":
    app.run()
