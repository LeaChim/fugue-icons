from django import template
from django.utils.safestring import mark_safe
from django.utils.html import conditional_escape as esc

register = template.Library()

@register.simple_tag
def fugue(name):
    return mark_safe('<i class="fugue-icon %s"></i>' % esc(name))

